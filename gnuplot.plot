set term postscript eps enhanced color size 50cm,50cm
set output 'subsample4SOM_SW_1998-2009_2016-02-718510547.bin_spectres.eps'
set multiplot layout 10,10
set xlabel '' font 'Helvetica,10'
set bmargin 2
set ylabel '' font 'Helvetica,10'
set lmargin 2
set xrange [ *:* ]
set yrange [ *:* ]
set xtics in font 'Helvetica,10'
set ytics in font 'Helvetica,10'
unset key
set label 1 '1' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot1.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 1
set label 2 '2' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot2.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 2
set label 3 '3' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot3.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 3
set label 4 '4' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot4.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 4
set label 5 '5' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot5.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 5
set label 6 '6' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot6.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 6
set label 7 '7' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot7.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 7
set label 8 '8' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot8.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 8
set label 9 '9' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot9.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 9
set label 10 '10' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot10.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 10
set label 11 '11' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot11.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 11
set label 12 '12' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot12.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 12
set label 13 '13' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot13.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 13
set label 14 '14' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot14.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 14
set label 15 '15' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot15.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 15
set label 16 '16' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot16.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 16
set label 17 '17' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot17.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 17
set label 18 '18' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot18.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 18
set label 19 '19' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot19.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 19
set label 20 '20' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot20.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 20
set label 21 '21' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot21.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 21
set label 22 '22' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot22.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 22
set label 23 '23' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot23.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 23
set label 24 '24' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot24.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 24
set label 25 '25' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot25.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 25
set label 26 '26' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot26.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 26
set label 27 '27' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot27.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 27
set label 28 '28' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot28.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 28
set label 29 '29' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot29.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 29
set label 30 '30' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot30.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 30
set label 31 '31' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot31.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 31
set label 32 '32' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot32.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 32
set label 33 '33' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot33.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 33
set label 34 '34' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot34.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 34
set label 35 '35' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot35.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 35
set label 36 '36' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot36.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 36
set label 37 '37' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot37.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 37
set label 38 '38' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot38.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 38
set label 39 '39' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot39.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 39
set label 40 '40' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot40.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 40
set label 41 '41' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot41.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 41
set label 42 '42' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot42.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 42
set label 43 '43' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot43.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 43
set label 44 '44' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot44.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 44
set label 45 '45' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot45.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 45
set label 46 '46' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot46.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 46
set label 47 '47' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot47.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 47
set label 48 '48' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot48.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 48
set label 49 '49' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot49.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 49
set label 50 '50' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot50.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 50
set label 51 '51' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot51.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 51
set label 52 '52' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot52.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 52
set label 53 '53' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot53.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 53
set label 54 '54' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot54.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 54
set label 55 '55' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot55.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 55
set label 56 '56' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot56.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 56
set label 57 '57' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot57.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 57
set label 58 '58' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot58.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 58
set label 59 '59' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot59.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 59
set label 60 '60' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot60.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 60
set label 61 '61' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot61.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 61
set label 62 '62' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot62.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 62
set label 63 '63' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot63.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 63
set label 64 '64' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot64.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 64
set label 65 '65' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot65.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 65
set label 66 '66' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot66.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 66
set label 67 '67' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot67.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 67
set label 68 '68' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot68.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 68
set label 69 '69' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot69.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 69
set label 70 '70' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot70.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 70
set label 71 '71' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot71.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 71
set label 72 '72' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot72.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 72
set label 73 '73' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot73.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 73
set label 74 '74' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot74.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 74
set label 75 '75' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot75.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 75
set label 76 '76' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot76.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 76
set label 77 '77' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot77.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 77
set label 78 '78' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot78.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 78
set label 79 '79' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot79.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 79
set label 80 '80' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot80.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 80
set label 81 '81' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot81.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 81
set label 82 '82' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot82.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 82
set label 83 '83' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot83.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 83
set label 84 '84' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot84.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 84
set label 85 '85' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot85.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 85
set label 86 '86' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot86.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 86
set label 87 '87' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot87.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 87
set label 88 '88' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot88.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 88
set label 89 '89' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot89.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 89
set label 90 '90' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot90.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 90
set label 91 '91' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot91.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 91
set label 92 '92' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot92.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 92
set label 93 '93' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot93.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 93
set label 94 '94' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot94.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 94
set label 95 '95' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot95.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 95
set label 96 '96' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot96.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 96
set label 97 '97' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot97.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 97
set label 98 '98' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot98.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 98
set label 99 '99' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot99.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 99
set label 100 '100' at graph 0.025,.95 font 'Courrier-Bold,28'
plot 'tmp_plot100.txt' using 1:2:3  with yerrorbars lt 1 lw 3, ''  using 1:2 with line lt -1  
unset label 100
